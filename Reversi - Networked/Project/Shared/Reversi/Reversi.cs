﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.IO;

namespace Reversi
{
    public class BaseGame
    {
        public enum Player
        {
            One,
            Two,
            Empty,
            Watching,
        };

        public enum BoardState
        {
            Unavailable,
            Available
        };

        public enum Coordinates
        {
            X,
            Y
        };

        public enum GameStates
        {
            StartGame,
            Player1Turn,
            Player2Turn,
            Player1Win,
            Player2Win,
            DrawGame,
            PlayAgain,
        };

        public Player[,] board;
        public BoardState[,] boardState;

        public int boardSize = 8;
        public GameStates currentState;

        public int availableMoves = 0;

        public void InitBoard()
        {
            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    board[x, y] = Player.Empty;
                }
            }

            // Place the four pieces in the centre
            board[3, 3] = Player.One;
            board[4, 3] = Player.Two;
            board[3, 4] = Player.Two;
            board[4, 4] = Player.One;
        }

        public BaseGame()
        {
            board = new Player[boardSize, boardSize];
            boardState = new BoardState[boardSize, boardSize];
        }

        public void Display()
        {
            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    switch (board[y, x])
                    {
                        case Player.Empty:
                            Console.Write(" " + ((y * boardSize) + x).ToString() + " ");
                            break;

                        case Player.One:
                            Console.Write(" X ");
                            break;

                        case Player.Two:
                            Console.Write(" O ");
                            break;
                    }
                }

                Console.WriteLine();
            }
        }

        public bool IsEndOfGame()
        {
            return (currentState == GameStates.Player1Win)
                | (currentState == GameStates.Player2Win)
                | (currentState == GameStates.DrawGame);
        }
    }

    public class ServerGame : BaseGame
    {
        public ServerGame()
        {

        }

        public void InitGame()
        {
            InitBoard();

            currentState = GameStates.Player1Turn;
        }

        public bool Send(Socket player, BaseGame.Player playerID)
        {


            /*
            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);

            writer.Write((int)currentState);
            writer.Write((int)playerID);

            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    writer.Write((int)board[y, x]);
                }
            }
            
            player.Send(memory.GetBuffer());
             * */
            return true;
        }

        public Player GetActivePlayer()
        {
            if (currentState == GameStates.Player1Turn)
            {
                return Player.One;
            }
            else
            {
                return Player.Two;
            }
        }

        public bool Receive(Socket player)
        {
            byte[] buffer = new byte[4096];
            int length = player.Receive(buffer);

            MemoryStream memory = new MemoryStream(buffer, 0, length);
            BinaryReader reader = new BinaryReader(memory);

            String go = reader.ReadString();
            String[] split = go.Split(new Char[] { ' ' });

            return ProcessGo(split[0], split[1]);
        }

        public bool ProcessGo(String x, String y)
        {
            if (IsTurnValid(GetActivePlayer(), Convert.ToInt32(x), Convert.ToInt32(y)) == true)
            {
                PlacePiece(GetActivePlayer(), Convert.ToInt32(x), Convert.ToInt32(y));

                if (IsWon() == true)
                {
                    if (GetActivePlayer() == Player.One)
                    {
                        currentState = GameStates.Player1Win;
                    }
                    else
                    {
                        currentState = GameStates.Player2Win;
                    }
                }
                else
                {
                    if (IsGameADraw(GetActivePlayer()) == true)
                    {
                        currentState = GameStates.DrawGame;
                    }
                    else
                    {
                        if (currentState == GameStates.Player1Turn)
                        {
                            currentState = GameStates.Player2Turn;
                        }
                        else
                        {
                            currentState = GameStates.Player1Turn;
                        }
                    }
                }
            }

            return true;
        }

        /* Function to return the available moves for the player to take */
        // Checks each space on the entire board if it is valid and sets the BoardState
        // depending on the result (Available, Unavailable) as well as increment the counter
        // for the available moves variable
        // Also used when drawing the board as the slots that are available are rendered as
        // dark green circles to help the player see which moves are available
        public virtual int GetAvailableMoves(Player ePlayer)
        {
            availableMoves = 0;

            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    if (IsTurnValid(ePlayer, x, y) == true)
                    {
                        boardState[x, y] = BoardState.Available;
                        availableMoves += 1;
                    }

                    else
                        boardState[x, y] = BoardState.Unavailable;
                }
            }

            return availableMoves;
        }

        /* Function to return the number of pieces on the board that belong to the player */
        // Checks each slot on the board and increments to counter
        // Used for displaying the current score
        public int GetPlayerPieceCount(Player ePlayer)
        {
            int counter = 0;

            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    if (board[x, y] == ePlayer)
                        counter++;
                }
            }

            return counter;
        }

        /* Function which checks if the chosen slot on the board is valid */
        // This is done by getting the direction of any adjacent pieces that belong to the player
        // and then getting the end of the chain. If the end piece belongs to the player, the
        // turn is valid, else it is not
        public bool IsTurnValid(Player ePlayer, int x, int y)
        {
            if (board[x, y] == Player.Empty)
            {
                bool[] adjacentPieces = GetDirectionsOfAdjacentOpponentPieces(ePlayer, x, y);

                for (int i = 0; i < adjacentPieces.Length; i++)
                {
                    if (adjacentPieces[i] == true)
                    {
                        int xEnd = GetEndOfOpponentChain(ePlayer, x, y, i, false, Coordinates.X);
                        int yEnd = GetEndOfOpponentChain(ePlayer, x, y, i, false, Coordinates.Y);

                        if (xEnd != -1 || yEnd != -1)
                            return true;
                    }
                }

                return false;
            }

            else
                return false;
        }

        /* Function which returns a boolean array with a value set to true if that space belongs
           to the opponent */
        public bool[] GetDirectionsOfAdjacentOpponentPieces(Player ePlayer, int x, int y)
        {
            bool[] adjacentPieces = new bool[8];

            // Get all directions in which there is an opponents piece adjacent the the current one

            if (y > 0)
            {
                if (x > 0)
                    // Upper left
                    if (board[x - 1, y - 1] != ePlayer && board[x - 1, y - 1] != Player.Empty)
                        adjacentPieces[0] = true;

                // Up
                if (board[x, y - 1] != ePlayer && board[x, y - 1] != Player.Empty)
                    adjacentPieces[1] = true;

                if (x < boardSize - 1)
                    // Upper right
                    if (board[x + 1, y - 1] != ePlayer && board[x + 1, y - 1] != Player.Empty)
                        adjacentPieces[2] = true;
            }

            if (x > 0)
                // Left
                if (board[x - 1, y] != ePlayer && board[x - 1, y] != Player.Empty)
                    adjacentPieces[3] = true;

            if (x < boardSize - 1)
                // Right
                if (board[x + 1, y] != ePlayer && board[x + 1, y] != Player.Empty)
                    adjacentPieces[4] = true;

            if (y < boardSize - 1)
            {
                if (x > 0)
                    // Lower Left
                    if (board[x - 1, y + 1] != ePlayer && board[x - 1, y + 1] != Player.Empty)
                        adjacentPieces[5] = true;

                // Down
                if (board[x, y + 1] != ePlayer && board[x, y + 1] != Player.Empty)
                    adjacentPieces[6] = true;

                if (x < boardSize - 1)
                    // Lower Right
                    if (board[x + 1, y + 1] != ePlayer && board[x + 1, y + 1] != Player.Empty)
                        adjacentPieces[7] = true;
            }

            return adjacentPieces;
        }

        /* Function which finds the end position of the chosen chain of pieces */
        // The direction is passed through which is then used to create a direction vector.
        // These values are then added onto the current x and y position to to get the new
        // position. This space is then checked to see if it belongs to the player. If it does,
        // that is the end of the chain. If it belongs to the opponent, the function is called 
        // again with the new variables until it either reaches the end of the board, an empty
        // space, or a piece belonging to the player.
        public int GetEndOfOpponentChain(Player ePlayer, int x, int y, int direction,
                                        bool flip, Coordinates returnValue)
        {
            int xDirection = 0, yDirection = 0;
            int xCurrent = x, yCurrent = y;

            // Get the direction vector
            switch (direction)
            {
                case 0:
                    xDirection = -1;
                    yDirection = -1;
                    break;

                case 1:
                    xDirection = 0;
                    yDirection = -1;
                    break;

                case 2:
                    xDirection = 1;
                    yDirection = -1;
                    break;

                case 3:
                    xDirection = -1;
                    yDirection = 0;
                    break;

                case 4:
                    xDirection = 1;
                    yDirection = 0;
                    break;

                case 5:
                    xDirection = -1;
                    yDirection = 1;
                    break;

                case 6:
                    xDirection = 0;
                    yDirection = 1;
                    break;

                case 7:
                    xDirection = 1;
                    yDirection = 1;
                    break;
            }

            // Get the next space in that direction
            xCurrent += xDirection;
            yCurrent += yDirection;

            // If that space is outside the board, return -1
            if (xCurrent < 0 || xCurrent >= boardSize || yCurrent < 0 || yCurrent >= boardSize)
                return -1;

            // Else if that space belongs to the player
            else if (board[xCurrent, yCurrent] == ePlayer)
            {
                // If the flip variable is true, it means the function is being called in the
                // FlipPieces function so the current space is set to now belong to the player
                if (flip == true)
                {
                    int xFlip = xCurrent;
                    int yFlip = yCurrent;

                    while (board[xFlip - xDirection, yFlip - yDirection] != ePlayer &&
                            board[xFlip - xDirection, yFlip - yDirection] != Player.Empty)
                    {
                        xFlip -= xDirection;
                        yFlip -= yDirection;

                        board[xFlip, yFlip] = ePlayer;
                    }
                }

                // return the position depending on the return value chosen
                switch (returnValue)
                {
                    case Coordinates.X:
                        return xCurrent;

                    case Coordinates.Y:
                        return yCurrent;
                }
            }

            // Else if that space is empty, return -1
            else if (board[xCurrent, yCurrent] == Player.Empty)
                return -1;

            // Else if that space belongs to the opponent, call this function again 
            // with the new location
            else
            {
                if (flip == true)
                    return GetEndOfOpponentChain(ePlayer, xCurrent, yCurrent,
                                                direction, true, returnValue);

                else
                    return GetEndOfOpponentChain(ePlayer, xCurrent, yCurrent,
                                                direction, false, returnValue);
            }

            return -1;
        }

        /* Function to place a piece */
        // If the chosen space is a valid slot, assign that slot to the player
        // and flip over the opponents pieces correctly
        public void PlacePiece(Player ePlayer, int x, int y)
        {
            if (IsTurnValid(ePlayer, x, y) == true)
            {
                board[x, y] = ePlayer;
                FlipPieces(ePlayer, x, y);
            }
        }

        /* Function to flip the opponents pieces correctly */
        // This is done by getting the direction of any adjacent pieces belonging to the opponent, 
        // then checking each slot in that direction and if the end piece belongs to the player,
        // flip them over.
        // This function is called after the player has taken their current turn
        public void FlipPieces(Player ePlayer, int x, int y)
        {
            bool[] adjacentPieces = GetDirectionsOfAdjacentOpponentPieces(ePlayer, x, y);

            for (int i = 0; i < adjacentPieces.Length; i++)
            {
                if (adjacentPieces[i] == true)
                {
                    int xEnd = GetEndOfOpponentChain(ePlayer, x, y, i, true, Coordinates.X);
                    int yEnd = GetEndOfOpponentChain(ePlayer, x, y, i, true, Coordinates.Y);
                }
            }
        }

        public bool IsWon(Player ePlayer)
        {
            if (GetAvailableMoves(Player.One) == 0 && GetAvailableMoves(Player.Two) == 0)            
            {
                if (ePlayer == Player.One)
                {
                    if (GetPlayerPieceCount(ePlayer) > GetPlayerPieceCount(Player.Two))
                        return true;

                    else
                        return false;
                }

                else if (ePlayer == Player.Two)
                {
                    if (GetPlayerPieceCount(ePlayer) > GetPlayerPieceCount(Player.One))
                        return true;

                    else
                        return false;
                }

                else
                    return false;
            }

            else
                return false;
        }

        public bool IsWon()
        {
            return IsWon(Player.One) || IsWon(Player.Two);
        }

        public bool IsGameADraw(Player ePlayer)
        {
            if (GetAvailableMoves(ePlayer) == 0)
            {
                if (ePlayer == Player.One)
                {
                    if (GetPlayerPieceCount(ePlayer) == GetPlayerPieceCount(Player.Two))
                        return true;

                    else
                        return false;
                }

                else
                {
                    if (GetPlayerPieceCount(ePlayer) == GetPlayerPieceCount(Player.One))
                        return true;

                    else
                        return false;
                }
            }

            else
                return false;
        }
    }

    public class ClientGame : BaseGame
    {
        public Player PlayerID;
        public ClientGame()
        {
            PlayerID = Player.Empty;

            InitGame();
        }

        public void InitGame()
        {
            InitBoard();
            currentState = GameStates.StartGame;
        }

        public bool ReceiveServerInput(Socket server)
        {
            byte[] buffer = new byte[4096];
            int length = server.Receive(buffer);

            MemoryStream memory = new MemoryStream(buffer, 0, length);
            BinaryReader reader = new BinaryReader(memory);

            currentState = (GameStates)reader.ReadInt32();
            PlayerID = (Player)reader.ReadInt32();

            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    board[x, y] = (Player)reader.ReadInt32();
                    boardState[x, y] = (BoardState)reader.ReadInt32();
                }
            }

            return true;
        }

        public bool IsMyGo()
        {
            return (PlayerID == Player.One && currentState == GameStates.Player1Turn)
                | (PlayerID == Player.Two && currentState == GameStates.Player2Turn);
        }

        public bool IsDraw()
        {
            return currentState == GameStates.DrawGame;
        }

        public bool DidIWin()
        {
            return (PlayerID == Player.One && currentState == GameStates.Player1Win)
                | (PlayerID == Player.Two && currentState == GameStates.Player2Win);
        }

        public bool ProcessInput(Socket server)
        {
            String go = Console.ReadLine();

            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);

            writer.Write(go);
            server.Send(memory.GetBuffer());

            return true;
        }
    }
}