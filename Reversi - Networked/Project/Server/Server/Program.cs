﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

using MessageTypes;
using Reversi;


namespace Server
{
    public class Server
    {
        public Dictionary<String, Socket> clientDictionary = new Dictionary<String, Socket>();
        public int clientID = 1;

        public Socket player1;
        public Socket player2;
        public ServerGame myGame;

        public void SendClientName(Socket s, String clientName)
        {
            ClientNameMsg nameMsg = new ClientNameMsg();
            nameMsg.name = clientName;

            MemoryStream outStream = nameMsg.WriteData();

            s.Send(outStream.GetBuffer());
        }

        public void SendChatMessage(String msg)
        {
            PublicChatMsg chatMsg = new PublicChatMsg();

            chatMsg.msg = msg;

            MemoryStream outStream = chatMsg.WriteData();

            lock (clientDictionary)
            {
                foreach (KeyValuePair<String, Socket> s in clientDictionary)
                {
                    try
                    {
                        s.Value.Send(outStream.GetBuffer());
                    }
                    catch (System.Exception)
                    {

                    }
                }
            }
        }

        public void SendPrivateMessage(Socket s, String from, String msg)
        {
            PrivateChatMsg chatMsg = new PrivateChatMsg();
            chatMsg.msg = msg;
            chatMsg.destination = from;
            MemoryStream outStream = chatMsg.WriteData();

            try
            {
                s.Send(outStream.GetBuffer());
            }
            catch (System.Exception)
            {

            }
        }

        public Socket GetSocketFromName(String name)
        {
            lock (clientDictionary)
            {
                return clientDictionary[name];
            }
        }

        public String GetNameFromSocket(Socket s)
        {
            lock (clientDictionary)
            {
                foreach (KeyValuePair<String, Socket> o in clientDictionary)
                {
                    if (o.Value == s)
                    {
                        return o.Key;
                    }
                }
            }

            return null;
        }

        public void RemoveClientBySocket(Socket s)
        {
            string name = GetNameFromSocket(s);

            if (name != null)
            {
                lock (clientDictionary)
                {
                    clientDictionary.Remove(name);
                }
            }
        }


        public void receiveClientProcess(Object o)
        {
            bool bQuit = false;

            Socket chatClient = (Socket)o;

            Console.WriteLine(GetNameFromSocket(chatClient) + " has connected.");

            while (bQuit == false)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int result;

                    result = chatClient.Receive(buffer);

                    if (result > 0)
                    {
                        MemoryStream stream = new MemoryStream(buffer);
                        BinaryReader read = new BinaryReader(stream);

                        Msg m = Msg.DecodeStream(read);

                        if (m != null)
                        {
                            Console.WriteLine("Got message: " + m.mID);
                            switch (m.mID)
                            {
                                case PublicChatMsg.ID:
                           
                                    {
                                        PublicChatMsg publicMsg = (PublicChatMsg)m;

                                        String formattedMsg = "<" + GetNameFromSocket(chatClient) + "> " + publicMsg.msg;

                                        Console.WriteLine("public chat - " + formattedMsg);

                                        SendChatMessage(formattedMsg);
                                    }
                                    break;

                                case PrivateChatMsg.ID:
                                    {
                                        PrivateChatMsg privateMsg = (PrivateChatMsg)m;

                                        String formattedMsg = "PRIVATE <" + GetNameFromSocket(chatClient) + "> " + privateMsg.msg;

                                        Console.WriteLine("private chat - " + formattedMsg + "to " + privateMsg.destination);

                                        SendPrivateMessage(GetSocketFromName(privateMsg.destination), GetNameFromSocket(chatClient), formattedMsg);

                                        formattedMsg = "<" + GetNameFromSocket(chatClient) + "> --> <" + privateMsg.destination + "> " + privateMsg.msg;
                                        SendPrivateMessage(chatClient, "", formattedMsg);
                                    }
                                    break;

                                case ReversiPlayerGoMsg.ID:
                                    {
                                        ReversiPlayerGoMsg playerGo = (ReversiPlayerGoMsg)m;
                                        Console.WriteLine("Player Go: " + playerGo.msg);

                                        String go = playerGo.msg;
                                        string[] split = go.Split(new Char[] { ' ' });

                                        myGame.ProcessGo(split[0], split[1]);
                                        SendGameState();

                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    bQuit = true;

                    String output = GetNameFromSocket(chatClient) + " has disconnected!";
                    Console.WriteLine(output);
                    SendChatMessage(output);

                    if (GetNameFromSocket(chatClient) == "Player 1")
                        myGame.currentState = BaseGame.GameStates.Player2Win;

                    else if (GetNameFromSocket(chatClient) == "Player 2")
                        myGame.currentState = BaseGame.GameStates.Player1Win;

                    SendGameState();

                    RemoveClientBySocket(chatClient);

                    chatClient = null;
                }
            }
        }

        public void SendGameState()
        {
            ReversiGameStateMsg gameMsg = new ReversiGameStateMsg();

            gameMsg.currentState = myGame.currentState;
            gameMsg.board = myGame.board;
            gameMsg.boardState = myGame.boardState;


            lock (clientDictionary)
            {
                foreach (KeyValuePair<String, Socket> kvp in clientDictionary)
                {
                    gameMsg.playerID = BaseGame.Player.Watching;

                    if (kvp.Value == player1)
                    {
                        gameMsg.playerID = BaseGame.Player.One;
                    }
                    else
                    {
                        if (kvp.Value == player2)
                        {
                            gameMsg.playerID = BaseGame.Player.Two;
                        }
                    }

                    try
                    {
                        MemoryStream outStream = gameMsg.WriteData();
                        kvp.Value.Send(outStream.GetBuffer());
                    }
                    catch (System.Exception)
                    {

                    }
                }
            }
        }
    }

    class Program
    {
        static List<Server> serverGames = new List<Server>();

        static void Main(string[] args)
        {
            Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            serverSocket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8500));
            serverSocket.Listen(32);

            bool bQuit = false;

            Console.WriteLine("Server: " + serverSocket.LocalEndPoint);

            int currentGame = 0;

            while (!bQuit)
            {
                Socket serverClient = serverSocket.Accept();

                currentGame = serverGames.Count - 1;

                // If there are no empty slots in any of the active games, create a new game
                if (serverGames.Count == 0 || serverGames[currentGame].clientID > 2)
                    serverGames.Add(new Server());

                // Get the counter of the last game in the list
                currentGame = serverGames.Count - 1;

                // If there aren't any other players in that game and the game is not yet initialised, this is Player 1
                if (serverGames[currentGame].clientDictionary.Count == 0 && serverGames[currentGame].myGame == null)
                {
                    serverGames[currentGame].player1 = serverClient;

                    serverGames[currentGame].myGame = new ServerGame();
                }


                // If Player 1 left before Player 2 has joined, Reinitialise the game
                else if (serverGames[currentGame].clientDictionary.Count == 0 && serverGames[currentGame].myGame != null)
                {
                    serverGames[currentGame].clientID = 1;
                    serverGames[currentGame].player1 = serverClient;

                    serverGames[currentGame].myGame = new ServerGame();
                }

                // Else if the Player 2 slot is still empty, this is Player 2
                else if (serverGames[currentGame].player2 == null && serverGames[currentGame].clientDictionary.Count != 0)
                {
                    serverGames[currentGame].player2 = serverClient;

                    serverGames[currentGame].myGame.InitGame();
                }

                // If a player is currently joining a game
                if (serverClient != null)
                {
                    Thread myThread = new Thread(serverGames[currentGame].receiveClientProcess);
                    myThread.Start(serverClient);

                    lock (serverGames[currentGame].clientDictionary)
                    {
                        if (serverGames[currentGame].clientID <= 2)
                        {
                            String clientName;

                            clientName = "Player " + serverGames[currentGame].clientID;


                            serverGames[currentGame].clientDictionary.Add(clientName, serverClient);

                            serverGames[currentGame].SendClientName(serverClient, clientName);
                            Thread.Sleep(500);

                            serverGames[currentGame].clientID++;
                        }
                    }
                }

                serverGames[currentGame].SendGameState();

                // If any of the games have an active game but no players, both players have left so remove that game
                for (int i = 0; i < serverGames.Count; i++)
                {
                    if (serverGames[i].clientDictionary.Count == 0 && serverGames[i].myGame != null)
                    {
                        serverGames.RemoveAt(i);
                        Console.WriteLine("Game " + i + " has ended.");
                    }
                }
            }
        }
    }
}

