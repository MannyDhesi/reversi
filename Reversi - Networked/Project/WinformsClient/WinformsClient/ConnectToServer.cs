﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinformsClient
{
    public partial class ConnectToServer : Form
    {
        public string _address,_port;

        public ConnectToServer()
        {
            InitializeComponent();

            this.ControlBox = false;
        }

        private void button_Connect_Click(object sender, EventArgs e)
        {
            if (maskedTextBox_Address.Text == string.Empty)
            {
                label_Error.Text = "Please type a valid IP Address!";
                return;
            }

            if (maskedTextBox_Port.Text == string.Empty)
            {
                label_Error.Text = "Please type a valid Port!";
                return;
            }

            _address = maskedTextBox_Address.Text;
            _port = maskedTextBox_Port.Text;

            Close();
        }

        private void button_Exit_Click(object sender, EventArgs e)
        {
            //Close();
            Application.Exit();
        }
    }
}
