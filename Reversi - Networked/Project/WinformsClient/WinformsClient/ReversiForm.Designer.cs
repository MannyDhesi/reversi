﻿namespace WinformsClient
{
    partial class ReversiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gameDisplay = new System.Windows.Forms.TextBox();
            this.gameInfo = new System.Windows.Forms.TextBox();
            this.playerScores = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox_Output = new System.Windows.Forms.TextBox();
            this.textBox_Input = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.textBox_ClientName = new System.Windows.Forms.TextBox();
            this.gameTitle = new System.Windows.Forms.Label();
            this.button_Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // gameDisplay
            // 
            this.gameDisplay.BackColor = System.Drawing.Color.Gray;
            this.gameDisplay.Cursor = System.Windows.Forms.Cursors.Default;
            this.gameDisplay.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameDisplay.Location = new System.Drawing.Point(11, 512);
            this.gameDisplay.Multiline = true;
            this.gameDisplay.Name = "gameDisplay";
            this.gameDisplay.ReadOnly = true;
            this.gameDisplay.Size = new System.Drawing.Size(484, 52);
            this.gameDisplay.TabIndex = 73;
            this.gameDisplay.TabStop = false;
            // 
            // gameInfo
            // 
            this.gameInfo.BackColor = System.Drawing.Color.Gray;
            this.gameInfo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameInfo.Location = new System.Drawing.Point(11, 571);
            this.gameInfo.Name = "gameInfo";
            this.gameInfo.ReadOnly = true;
            this.gameInfo.Size = new System.Drawing.Size(167, 26);
            this.gameInfo.TabIndex = 74;
            this.gameInfo.TabStop = false;
            // 
            // playerScores
            // 
            this.playerScores.BackColor = System.Drawing.Color.Gray;
            this.playerScores.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerScores.Location = new System.Drawing.Point(342, 570);
            this.playerScores.Name = "playerScores";
            this.playerScores.ReadOnly = true;
            this.playerScores.Size = new System.Drawing.Size(57, 26);
            this.playerScores.TabIndex = 75;
            this.playerScores.TabStop = false;
            this.playerScores.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(246, 570);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(90, 26);
            this.textBox1.TabIndex = 76;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "PLAYER 1";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.Black;
            this.textBox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(405, 570);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(90, 26);
            this.textBox2.TabIndex = 77;
            this.textBox2.TabStop = false;
            this.textBox2.Text = "PLAYER 2";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_Output
            // 
            this.textBox_Output.Location = new System.Drawing.Point(520, 75);
            this.textBox_Output.Multiline = true;
            this.textBox_Output.Name = "textBox_Output";
            this.textBox_Output.ReadOnly = true;
            this.textBox_Output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_Output.Size = new System.Drawing.Size(285, 415);
            this.textBox_Output.TabIndex = 78;
            this.textBox_Output.TabStop = false;
            // 
            // textBox_Input
            // 
            this.textBox_Input.Location = new System.Drawing.Point(520, 512);
            this.textBox_Input.Multiline = true;
            this.textBox_Input.Name = "textBox_Input";
            this.textBox_Input.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_Input.Size = new System.Drawing.Size(195, 52);
            this.textBox_Input.TabIndex = 79;
            // 
            // buttonSend
            // 
            this.buttonSend.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSend.Location = new System.Drawing.Point(721, 512);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(84, 52);
            this.buttonSend.TabIndex = 80;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // textBox_ClientName
            // 
            this.textBox_ClientName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_ClientName.Location = new System.Drawing.Point(520, 40);
            this.textBox_ClientName.Name = "textBox_ClientName";
            this.textBox_ClientName.ReadOnly = true;
            this.textBox_ClientName.Size = new System.Drawing.Size(100, 26);
            this.textBox_ClientName.TabIndex = 81;
            this.textBox_ClientName.TabStop = false;
            // 
            // gameTitle
            // 
            this.gameTitle.AutoSize = true;
            this.gameTitle.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameTitle.ForeColor = System.Drawing.Color.White;
            this.gameTitle.Location = new System.Drawing.Point(650, 22);
            this.gameTitle.Name = "gameTitle";
            this.gameTitle.Size = new System.Drawing.Size(155, 44);
            this.gameTitle.TabIndex = 82;
            this.gameTitle.Text = "Reversi";
            this.gameTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_Close
            // 
            this.button_Close.Enabled = false;
            this.button_Close.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Close.Location = new System.Drawing.Point(721, 571);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(84, 25);
            this.button_Close.TabIndex = 83;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Visible = false;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // ReversiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(831, 608);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.gameTitle);
            this.Controls.Add(this.textBox_ClientName);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.textBox_Input);
            this.Controls.Add(this.textBox_Output);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.playerScores);
            this.Controls.Add(this.gameInfo);
            this.Controls.Add(this.gameDisplay);
            this.Name = "ReversiForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reversi";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox gameDisplay;
        private System.Windows.Forms.TextBox gameInfo;
        private System.Windows.Forms.TextBox playerScores;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox_Output;
        private System.Windows.Forms.TextBox textBox_Input;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.TextBox textBox_ClientName;
        private System.Windows.Forms.Label gameTitle;
        private System.Windows.Forms.Button button_Close;

    }
}

