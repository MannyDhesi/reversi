﻿namespace WinformsClient
{
    partial class ConnectToServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button_Connect = new System.Windows.Forms.Button();
            this.label_Error = new System.Windows.Forms.Label();
            this.maskedTextBox_Address = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBox_Port = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_Exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter Server IP Address:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_Connect
            // 
            this.button_Connect.Location = new System.Drawing.Point(105, 194);
            this.button_Connect.Name = "button_Connect";
            this.button_Connect.Size = new System.Drawing.Size(75, 23);
            this.button_Connect.TabIndex = 2;
            this.button_Connect.Text = "Connect!";
            this.button_Connect.UseVisualStyleBackColor = true;
            this.button_Connect.Click += new System.EventHandler(this.button_Connect_Click);
            // 
            // label_Error
            // 
            this.label_Error.Location = new System.Drawing.Point(12, 168);
            this.label_Error.Name = "label_Error";
            this.label_Error.Size = new System.Drawing.Size(260, 23);
            this.label_Error.TabIndex = 3;
            this.label_Error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // maskedTextBox_Address
            // 
            this.maskedTextBox_Address.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Insert;
            this.maskedTextBox_Address.Location = new System.Drawing.Point(12, 42);
            this.maskedTextBox_Address.Mask = "000.000.000.000";
            this.maskedTextBox_Address.Name = "maskedTextBox_Address";
            this.maskedTextBox_Address.Size = new System.Drawing.Size(259, 20);
            this.maskedTextBox_Address.TabIndex = 6;
            this.maskedTextBox_Address.Text = "127000000001";
            this.maskedTextBox_Address.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // maskedTextBox_Port
            // 
            this.maskedTextBox_Port.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.maskedTextBox_Port.Location = new System.Drawing.Point(12, 118);
            this.maskedTextBox_Port.Mask = "0000";
            this.maskedTextBox_Port.Name = "maskedTextBox_Port";
            this.maskedTextBox_Port.Size = new System.Drawing.Size(259, 20);
            this.maskedTextBox_Port.TabIndex = 8;
            this.maskedTextBox_Port.Text = "8500";
            this.maskedTextBox_Port.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(260, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "Enter Server Port:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_Exit
            // 
            this.button_Exit.Location = new System.Drawing.Point(105, 237);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(75, 23);
            this.button_Exit.TabIndex = 9;
            this.button_Exit.Text = "Exit";
            this.button_Exit.UseVisualStyleBackColor = true;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // ConnectToServer
            // 
            this.AcceptButton = this.button_Connect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 272);
            this.ControlBox = false;
            this.Controls.Add(this.button_Exit);
            this.Controls.Add(this.maskedTextBox_Port);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.maskedTextBox_Address);
            this.Controls.Add(this.label_Error);
            this.Controls.Add(this.button_Connect);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ConnectToServer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reversi";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_Connect;
        private System.Windows.Forms.Label label_Error;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_Address;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_Port;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_Exit;
    }
}