﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

using MessageTypes;
using Reversi;

namespace WinformsClient
{
    public partial class ReversiForm : Form
    {
        Socket client;
        private Thread myThread;
        bool bQuit = false;
        bool bConnected = false;
        bool bServerLost = false;

        ClientGameWinform clientGame;
        ClientChatWinform clientChat;

        string address, port;

        static void clientProcess(Object o)
        {
            ReversiForm form = (ReversiForm)o;

            form.clientChat = new ClientChatWinform();
            form.clientGame = new ClientGameWinform();

            form.clientChat.Init();
            form.clientGame.Init();

            while ((form.bConnected == false) && (form.bQuit == false))
            {
                try
                {
                    form.client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    form.client.Connect(new IPEndPoint(IPAddress.Parse(form.address), Convert.ToInt32(form.port)));
                    form.bConnected = true;

                    form.ClearChatText();
                    form.AddChatText("Connected to server: " + form.client.RemoteEndPoint);
                    form.AddChatText("\nWelcome to chat!\n");

                    form.UpdateBoard();

                    Thread receiveThread;

                    receiveThread = new Thread(clientReceive);
                    receiveThread.Start(o);

                    while ((form.bQuit == false) && (form.bConnected == true))
                    {
                        if (form.IsDisposed == true)
                        {
                            form.bQuit = true;
                            form.client.Close();
                        }
                    }

                    receiveThread.Abort();
                }
                catch (System.Exception)
                {
                    if (form.bConnected == false && form.clientGame.currentState == BaseGame.GameStates.StartGame)
                    {
                        //form.ClearChatText();

                        using (ConnectToServer connectToServer = new ConnectToServer())
                        {
                            connectToServer.ShowDialog();

                            form.address = connectToServer._address;
                            form.port = connectToServer._port;
                        }

                        form.AddChatText("Server cannot be found!");
                    }

                    else
                    {
                        if (form.bServerLost == false)
                        {
                            form.AddChatText("Connection to the server has been lost!");
                            form.AddChatText("Game has ended as a draw!");
                            form.clientGame.currentState = BaseGame.GameStates.DrawGame;
                            form.bServerLost = true;
                        }

                        form.UpdateBoard();
                    }
                }
            }
        }

        static void clientReceive(Object o)
        {
            ReversiForm form = (ReversiForm)o;

            while (form.bConnected == true)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int result;

                    result = form.client.Receive(buffer);

                    if (result > 0)
                    {
                        MemoryStream stream = new MemoryStream(buffer);
                        BinaryReader read = new BinaryReader(stream);

                        Msg m = Msg.DecodeStream(read);

                        if (m != null)
                        {
                            Console.Write("Got a message: " + m.mID);
                            switch (m.mID)
                            {
                                case PublicChatMsg.ID:
                                    {
                                        PublicChatMsg publicMsg = (PublicChatMsg)m;

                                        form.AddChatText(publicMsg.msg);
                                    }
                                    break;

                                case ClientNameMsg.ID:
                                    {
                                        ClientNameMsg clientName = (ClientNameMsg)m;

                                        form.SetClientName(clientName.name);
                                    }
                                    break;

                                case ReversiGameStateMsg.ID:
                                    {
                                        ReversiGameStateMsg boardState = (ReversiGameStateMsg)m;

                                        lock (form.clientGame)
                                        {
                                            form.clientGame.board = boardState.board;
                                            form.clientGame.boardState = boardState.boardState;
                                            form.clientGame.currentState = boardState.currentState;
                                            form.clientGame.PlayerID = boardState.playerID;
                                        }

                                        form.UpdateBoard();
                                    }
                                    break;

                                default:
                                    throw new Exception("Unknown message type");
                                    break;
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    form.bConnected = false;
                    form.clientGame.currentState = BaseGame.GameStates.DrawGame;
                }

            }
        }

        static int boardSize = 8;

        private Button[,] board = new Button[boardSize, boardSize];

        public ReversiForm()
        {
            InitializeComponent();

            ControlBox = false;

            // Create 8x8 grid using buttons in 2D array
            int i = 12;
            int j = 12;

            for (int y = 0; y < boardSize; y++)
            {
                i = 12;

                for (int x = 0; x < boardSize; x++)
                {
                    board[x, y] = new Button();
                    board[x, y].Name = "button " + x + " " + y;
                    board[x, y].Location = new Point(j, i);
                    board[x, y].Size = new Size(50, 50);
                    board[x, y].BackColor = Color.Green;
                    board[x, y].Font = new Font("Arial", 27, FontStyle.Bold);
                    board[x, y].TextAlign = ContentAlignment.MiddleCenter;
                    board[x, y].Cursor = Cursors.Default;
                    board[x, y].Visible = true;
                    board[x, y].Enabled = true;
                    board[x, y].Click += new EventHandler(button_Click);
                    Controls.Add(board[x, y]);

                    i += 62;
                }

                j += 62;
            }

            myThread = new Thread(clientProcess);
            myThread.Start(this);

            Application.ApplicationExit += delegate { OnExit(); };
        }

        private delegate void AddChatTextDelegate(String s);

        private void AddChatText(String s)
        {
            try
            {
                if (textBox_Output.InvokeRequired)
                {
                    Invoke(new AddChatTextDelegate(AddChatText), new object[] { s });
                }
                else
                {
                    textBox_Output.Text += s;
                    textBox_Output.Text += Environment.NewLine;
                }
            }

            catch (Exception)
            {

            }
        }

        private delegate void ClearChatTextDelegate();

        private void ClearChatText()
        {
            try
            {
                if (textBox_Output.InvokeRequired)
                {
                    Invoke(new ClearChatTextDelegate(ClearChatText));
                }
                else
                {
                    textBox_Output.Clear();
                }
            }

            catch (Exception)
            {

            }
        }

        private delegate void AddGameDisplayTextDelegate(String s);

        private void AddGameDisplayText(String s)
        {
            try
            {
                if (gameDisplay.InvokeRequired)
                {
                    Invoke(new AddGameDisplayTextDelegate(AddGameDisplayText), new object[] { s });
                }
                else
                {
                    gameDisplay.Text = s;
                }
            }

            catch (Exception)
            {

            }
        }

        private delegate void SetClientNameDelegate(String s);
        private void SetClientName(String s)
        {
            if (this.InvokeRequired)
            {
                Invoke(new SetClientNameDelegate(SetClientName), new object[] { s });
            }
            else
            {
                textBox_ClientName.Text = s;
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if ((textBox_Input.Text.Length > 0) && (client != null))
            {
                try
                {
                    PublicChatMsg publicMsg = new PublicChatMsg();

                    publicMsg.msg = textBox_Input.Text;
                    MemoryStream outStream = publicMsg.WriteData();
                    client.Send(outStream.GetBuffer());
                }
                catch (System.Exception)
                {
                }

                textBox_Input.Text = "";
            }
        }

        private void OnExit()
        {
            bQuit = true;
            Thread.Sleep(500);
            if (myThread != null)
            {
                myThread.Abort();
            }
        }

        public void SetInput(int x, int y)
        {
            if (IsTurnValid(clientGame.PlayerID, x, y) == true)
            {
                ReversiPlayerGoMsg goMsg = new ReversiPlayerGoMsg();

                goMsg.msg = x.ToString() + " " + y.ToString();

                MemoryStream outStream = goMsg.WriteData();

                try
                {
                    client.Send(outStream.GetBuffer());
                }
                catch (System.Exception)
                {

                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateBoard();
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (clientGame.IsMyGo())
            {
                // Get the name of the button and split it when there is a space
                string name = ((Button)sender).Name;
                string[] split = name.Split(new Char[] { ' ' });

                SetInput(Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));

                UpdateBoard();
            }
        }

        public void SetButtons(bool bEnabled)
        {
            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    board[x, y].Enabled = bEnabled;
                }
            }
        }
        public int GetAvailableMoves(BaseGame.Player ePlayer)
        {
            clientGame.availableMoves = 0;

            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    if (IsTurnValid(ePlayer, x, y) == true)
                    {
                        clientGame.boardState[x, y] = BaseGame.BoardState.Available;
                        clientGame.availableMoves += 1;
                    }

                    else
                        clientGame.boardState[x, y] = BaseGame.BoardState.Unavailable;
                }
            }

            return clientGame.availableMoves;
        }

        /* Function to return the number of pieces on the board that belong to the player */
        // Checks each slot on the board and increments to counter
        // Used for displaying the current score
        public int GetPlayerPieceCount(BaseGame.Player ePlayer)
        {
            int counter = 0;

            for (int y = 0; y < boardSize; y++)
            {
                for (int x = 0; x < boardSize; x++)
                {
                    if (clientGame.board[x, y] == ePlayer)
                        counter++;
                }
            }

            return counter;
        }

        /* Function which checks if the chosen slot on the board is valid */
        // This is done by getting the direction of any adjacent pieces that belong to the player
        // and then getting the end of the chain. If the end piece belongs to the player, the
        // turn is valid, else it is not
        public bool IsTurnValid(BaseGame.Player ePlayer, int x, int y)
        {
            if (clientGame.board[x, y] == BaseGame.Player.Empty)
            {
                bool[] adjacentPieces = GetDirectionsOfAdjacentOpponentPieces(ePlayer, x, y);

                for (int i = 0; i < adjacentPieces.Length; i++)
                {
                    if (adjacentPieces[i] == true)
                    {
                        int xEnd = GetEndOfOpponentChain(ePlayer, x, y, i, false, BaseGame.Coordinates.X);
                        int yEnd = GetEndOfOpponentChain(ePlayer, x, y, i, false, BaseGame.Coordinates.Y);

                        if (xEnd != -1 || yEnd != -1)
                            return true;
                    }
                }

                return false;
            }

            else
                return false;
        }

        /* Function which returns a boolean array with a value set to true if that space belongs
           to the opponent */
        public bool[] GetDirectionsOfAdjacentOpponentPieces(BaseGame.Player ePlayer, int x, int y)
        {
            bool[] adjacentPieces = new bool[8];

            // Get all directions in which there is an opponents piece adjacent the the current one

            if (y > 0)
            {
                if (x > 0)
                    // Upper left
                    if (clientGame.board[x - 1, y - 1] != ePlayer && clientGame.board[x - 1, y - 1] != BaseGame.Player.Empty)
                        adjacentPieces[0] = true;

                // Up
                if (clientGame.board[x, y - 1] != ePlayer && clientGame.board[x, y - 1] != BaseGame.Player.Empty)
                    adjacentPieces[1] = true;

                if (x < boardSize - 1)
                    // Upper right
                    if (clientGame.board[x + 1, y - 1] != ePlayer && clientGame.board[x + 1, y - 1] != BaseGame.Player.Empty)
                        adjacentPieces[2] = true;
            }

            if (x > 0)
                // Left
                if (clientGame.board[x - 1, y] != ePlayer && clientGame.board[x - 1, y] != BaseGame.Player.Empty)
                    adjacentPieces[3] = true;

            if (x < boardSize - 1)
                // Right
                if (clientGame.board[x + 1, y] != ePlayer && clientGame.board[x + 1, y] != BaseGame.Player.Empty)
                    adjacentPieces[4] = true;

            if (y < boardSize - 1)
            {
                if (x > 0)
                    // Lower Left
                    if (clientGame.board[x - 1, y + 1] != ePlayer && clientGame.board[x - 1, y + 1] != BaseGame.Player.Empty)
                        adjacentPieces[5] = true;

                // Down
                if (clientGame.board[x, y + 1] != ePlayer && clientGame.board[x, y + 1] != BaseGame.Player.Empty)
                    adjacentPieces[6] = true;

                if (x < boardSize - 1)
                    // Lower Right
                    if (clientGame.board[x + 1, y + 1] != ePlayer && clientGame.board[x + 1, y + 1] != BaseGame.Player.Empty)
                        adjacentPieces[7] = true;
            }

            return adjacentPieces;
        }

        /* Function which finds the end position of the chosen chain of pieces */
        // The direction is passed through which is then used to create a direction vector.
        // These values are then added onto the current x and y position to to get the new
        // position. This space is then checked to see if it belongs to the player. If it does,
        // that is the end of the chain. If it belongs to the opponent, the function is called 
        // again with the new variables until it either reaches the end of the board, an empty
        // space, or a piece belonging to the player.
        public int GetEndOfOpponentChain(BaseGame.Player ePlayer, int x, int y, int direction,
                                        bool flip, BaseGame.Coordinates returnValue)
        {
            int xDirection = 0, yDirection = 0;
            int xCurrent = x, yCurrent = y;

            // Get the direction vector
            switch (direction)
            {
                case 0:
                    xDirection = -1;
                    yDirection = -1;
                    break;

                case 1:
                    xDirection = 0;
                    yDirection = -1;
                    break;

                case 2:
                    xDirection = 1;
                    yDirection = -1;
                    break;

                case 3:
                    xDirection = -1;
                    yDirection = 0;
                    break;

                case 4:
                    xDirection = 1;
                    yDirection = 0;
                    break;

                case 5:
                    xDirection = -1;
                    yDirection = 1;
                    break;

                case 6:
                    xDirection = 0;
                    yDirection = 1;
                    break;

                case 7:
                    xDirection = 1;
                    yDirection = 1;
                    break;
            }

            // Get the next space in that direction
            xCurrent += xDirection;
            yCurrent += yDirection;

            // If that space is outside the board, return -1
            if (xCurrent < 0 || xCurrent >= boardSize || yCurrent < 0 || yCurrent >= boardSize)
                return -1;

            // Else if that space belongs to the player
            else if (clientGame.board[xCurrent, yCurrent] == ePlayer)
            {
                // If the flip variable is true, it means the function is being called in the
                // FlipPieces function so the current space is set to now belong to the player
                if (flip == true)
                {
                    int xFlip = xCurrent;
                    int yFlip = yCurrent;

                    while (clientGame.board[xFlip - xDirection, yFlip - yDirection] != ePlayer &&
                            clientGame.board[xFlip - xDirection, yFlip - yDirection] != BaseGame.Player.Empty)
                    {
                        xFlip -= xDirection;
                        yFlip -= yDirection;

                        clientGame.board[xFlip, yFlip] = ePlayer;
                    }
                }

                // return the position depending on the return value chosen
                switch (returnValue)
                {
                    case BaseGame.Coordinates.X:
                        return xCurrent;

                    case BaseGame.Coordinates.Y:
                        return yCurrent;
                }
            }

            // Else if that space is empty, return -1
            else if (clientGame.board[xCurrent, yCurrent] == BaseGame.Player.Empty)
                return -1;

            // Else if that space belongs to the opponent, call this function again 
            // with the new location
            else
            {
                if (flip == true)
                    return GetEndOfOpponentChain(ePlayer, xCurrent, yCurrent,
                                                direction, true, returnValue);

                else
                    return GetEndOfOpponentChain(ePlayer, xCurrent, yCurrent,
                                                direction, false, returnValue);
            }

            return -1;
        }

        private delegate void SetBoardDelegate();

        public void UpdateBoard()
        {
            if (board[0, 0].InvokeRequired)
            {
                try
                {
                    Invoke(new SetBoardDelegate(UpdateBoard));
                }
                catch (System.Exception)
                {

                }
            }

            else
            {
                switch (clientGame.currentState)
                {
                    case BaseGame.GameStates.StartGame:
                        if (bConnected == true)
                        {
                            gameDisplay.Text = "Waiting for Opponent ...";
                            button_Close.Visible = true;
                            button_Close.Enabled = true;
                        }
                        else
                        {
                            gameDisplay.Text = "Waiting for Server ...";
                            button_Close.Visible = false;
                            button_Close.Enabled = false;
                        }
                        break;

                    case BaseGame.GameStates.Player1Turn:
                        gameDisplay.Text = "Player 1's Turn";
                        gameDisplay.BackColor = Color.White;
                        gameDisplay.ForeColor = Color.Black;
                        button_Close.Visible = false;
                        button_Close.Enabled = false;

                        if (GetAvailableMoves(BaseGame.Player.One) == 0)
                        {
                            if (GetAvailableMoves(BaseGame.Player.Two) != 0)
                            {
                                AddChatText("No available moves for Player 1");
                                goto case BaseGame.GameStates.Player2Turn;
                            }

                            else
                            {
                                if (GetPlayerPieceCount(BaseGame.Player.One) > GetPlayerPieceCount(BaseGame.Player.Two))
                                    goto case BaseGame.GameStates.Player1Win;

                                else if (GetPlayerPieceCount(BaseGame.Player.Two) > GetPlayerPieceCount(BaseGame.Player.One))
                                    goto case BaseGame.GameStates.Player2Win;

                                else
                                    goto case BaseGame.GameStates.DrawGame;
                            }
                        }
                        break;

                    case BaseGame.GameStates.Player2Turn:
                        gameDisplay.Text = "Player 2's Turn";
                        gameDisplay.BackColor = Color.Black;
                        gameDisplay.ForeColor = Color.White;
                        button_Close.Visible = false;
                        button_Close.Enabled = false;

                        if (GetAvailableMoves(BaseGame.Player.Two) == 0)
                        {
                            if (GetAvailableMoves(BaseGame.Player.One) != 0)
                            {
                                AddChatText("No available moves for Player 2");
                                goto case BaseGame.GameStates.Player1Turn;
                            }

                            else
                            {
                                if (GetPlayerPieceCount(BaseGame.Player.One) > GetPlayerPieceCount(BaseGame.Player.Two))
                                    goto case BaseGame.GameStates.Player1Win;

                                else if (GetPlayerPieceCount(BaseGame.Player.Two) > GetPlayerPieceCount(BaseGame.Player.One))
                                    goto case BaseGame.GameStates.Player2Win;

                                else
                                    goto case BaseGame.GameStates.DrawGame;
                            }
                        }
                        break;

                    case BaseGame.GameStates.Player1Win:
                        gameDisplay.Text = "Player 1 Wins!";
                        gameDisplay.BackColor = Color.White;
                        gameDisplay.ForeColor = Color.Black;
                        button_Close.Visible = true;
                        button_Close.Enabled = true;
                        break;

                    case BaseGame.GameStates.Player2Win:
                        gameDisplay.Text = "Player 2 Wins!";
                        gameDisplay.BackColor = Color.Black;
                        gameDisplay.ForeColor = Color.White;
                        button_Close.Visible = true;
                        button_Close.Enabled = true;
                        break;

                    case BaseGame.GameStates.DrawGame:
                        gameDisplay.Text = "Draw!";
                        gameDisplay.BackColor = Color.Gray;
                        button_Close.Visible = true;
                        button_Close.Enabled = true;
                        break;

                    case BaseGame.GameStates.PlayAgain:
                        clientGame.InitBoard();
                        goto case BaseGame.GameStates.StartGame;
                    //break;
                }
                
                if (bConnected == false)
                {
                    return;
                }

                // Get the text to show in the relevent text boxes
                if (clientGame.IsMyGo())
                {
                    GetAvailableMoves(clientGame.PlayerID);
                    gameInfo.Text = "Available Moves: " + clientGame.availableMoves;
                }

                else
                    gameInfo.Text = "Available Moves: -";

                if (clientGame.currentState != BaseGame.GameStates.StartGame)
                    playerScores.Text = GetPlayerPieceCount(BaseGame.Player.One) +
                        " : " + GetPlayerPieceCount(BaseGame.Player.Two);

                else
                    playerScores.Text = "- : -";


                // Set the colour of each space depending on who it belongs to
                for (int x = 0; x < boardSize; x++)
                {
                    for (int y = 0; y < boardSize; y++)
                    {
                        if (clientGame.currentState == BaseGame.GameStates.StartGame)
                        {
                            board[x, y].ForeColor = Color.Green;
                            board[x, y].Text = "";
                        }

                        else
                        {
                            if (clientGame.board[x, y] == BaseGame.Player.One)
                            {
                                board[x, y].ForeColor = Color.White;
                                board[x, y].Text = "O";
                            }

                            else if (clientGame.board[x, y] == BaseGame.Player.Two)
                            {
                                board[x, y].ForeColor = Color.Black;
                                board[x, y].Text = "O";
                            }

                            else
                            {
                                board[x, y].ForeColor = Color.Green;
                                board[x, y].Text = "";
                            }

                            if (clientGame.IsMyGo())
                            {
                                if (clientGame.boardState[x, y] == BaseGame.BoardState.Available)
                                {
                                    board[x, y].ForeColor = Color.DarkGreen;
                                    board[x, y].Text = "O";
                                }
                            }
                        }
                    }
                }
            }
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
