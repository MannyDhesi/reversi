﻿namespace Reversi
{
    partial class ReversiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playAgainButton = new System.Windows.Forms.Button();
            this.gameDisplay = new System.Windows.Forms.TextBox();
            this.gameInfo = new System.Windows.Forms.TextBox();
            this.playerScores = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // playAgainButton
            // 
            this.playAgainButton.Enabled = false;
            this.playAgainButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playAgainButton.Location = new System.Drawing.Point(354, 512);
            this.playAgainButton.Name = "playAgainButton";
            this.playAgainButton.Size = new System.Drawing.Size(141, 52);
            this.playAgainButton.TabIndex = 57;
            this.playAgainButton.Text = "Play Again?";
            this.playAgainButton.UseVisualStyleBackColor = true;
            this.playAgainButton.Visible = false;
            this.playAgainButton.Click += new System.EventHandler(this.playAgainButton_Click);
            // 
            // gameDisplay
            // 
            this.gameDisplay.BackColor = System.Drawing.Color.Gray;
            this.gameDisplay.Cursor = System.Windows.Forms.Cursors.Default;
            this.gameDisplay.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameDisplay.Location = new System.Drawing.Point(11, 512);
            this.gameDisplay.Multiline = true;
            this.gameDisplay.Name = "gameDisplay";
            this.gameDisplay.ReadOnly = true;
            this.gameDisplay.Size = new System.Drawing.Size(484, 52);
            this.gameDisplay.TabIndex = 73;
            this.gameDisplay.TabStop = false;
            // 
            // gameInfo
            // 
            this.gameInfo.BackColor = System.Drawing.Color.Gray;
            this.gameInfo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameInfo.Location = new System.Drawing.Point(11, 571);
            this.gameInfo.Name = "gameInfo";
            this.gameInfo.ReadOnly = true;
            this.gameInfo.Size = new System.Drawing.Size(167, 26);
            this.gameInfo.TabIndex = 74;
            this.gameInfo.TabStop = false;
            // 
            // playerScores
            // 
            this.playerScores.BackColor = System.Drawing.Color.Gray;
            this.playerScores.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerScores.Location = new System.Drawing.Point(342, 570);
            this.playerScores.Name = "playerScores";
            this.playerScores.ReadOnly = true;
            this.playerScores.Size = new System.Drawing.Size(57, 26);
            this.playerScores.TabIndex = 75;
            this.playerScores.TabStop = false;
            this.playerScores.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(246, 570);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(90, 26);
            this.textBox1.TabIndex = 76;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "PLAYER 1";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.Black;
            this.textBox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(405, 570);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(90, 26);
            this.textBox2.TabIndex = 77;
            this.textBox2.TabStop = false;
            this.textBox2.Text = "PLAYER 2";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ReversiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(507, 608);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.playerScores);
            this.Controls.Add(this.gameInfo);
            this.Controls.Add(this.playAgainButton);
            this.Controls.Add(this.gameDisplay);
            this.Name = "ReversiForm";
            this.Text = "Reversi";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button playAgainButton;
        private System.Windows.Forms.TextBox gameDisplay;
        private System.Windows.Forms.TextBox gameInfo;
        private System.Windows.Forms.TextBox playerScores;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;

    }
}

