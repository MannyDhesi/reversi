﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reversi
{
    public class BaseGame
    {
        public Board TheBoard;
        public int boardWidth = 8;
        public int boardHeight = 8;
        public string gameText = "";

        public enum GameStates
        {
            StartGame,
            Player1Turn,
            Player2Turn,
            Player1Win,
            Player2Win,
            DrawGame,
            PlayAgain,
        };

        public GameStates currentState;

        public BaseGame()
        {
            TheBoard = new Board(boardWidth, boardHeight);

            TheBoard.Initialise();
            currentState = GameStates.StartGame;
        }

        private void Restart()
        {
            TheBoard.Initialise();
        }
    }    

}
