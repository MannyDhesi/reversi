﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;

namespace Reversi
{
    public class WinformsGame : BaseGame
    {
        public Thread myThread;
        public bool bRun = false;
        public ReversiForm form;

        public bool Init(ReversiForm f)
        {
            form = f;

            bRun = true;

            while (bRun)
            {
                //myThread = new Thread(new ParameterizedThreadStart(ReceiveFunction));
                //myThread.Start(this);

                form.UpdateBoard();
            }

            return true;
        }
    }
}
