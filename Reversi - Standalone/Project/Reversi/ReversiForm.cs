﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;

namespace Reversi
{
    public partial class ReversiForm : Form
    {
        BaseGame game;

        static int boardSize = 8;

        private Button[,] board = new Button[boardSize, boardSize];

        public ReversiForm()
        {
            InitializeComponent();

            // Create 8x8 grid using buttons in 2D array
            int i = 12;
            int j = 12;

            for (int x = 0; x < boardSize; x++)
            {
                i = 12;

                for (int y = 0; y < boardSize; y++)
                {
                    board[x, y] = new Button();
                    board[x, y].Name = "button " + x + " " + y;
                    board[x, y].Location = new Point(j, i);
                    board[x, y].Size = new Size(50, 50);
                    board[x, y].BackColor = Color.Green;
                    board[x, y].Font = new Font("Arial", 27, FontStyle.Bold);
                    board[x, y].TextAlign = ContentAlignment.MiddleCenter;
                    board[x, y].Cursor = Cursors.Default;
                    board[x, y].Visible = true;
                    board[x, y].Enabled = true;
                    board[x, y].Click += new EventHandler(button_Click);
                    Controls.Add(board[x, y]);

                    i += 62;
                }

                j += 62;
            }

            game = new WinformsGame();

            UpdateBoard();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateBoard();
        }

        private void button_Click(object sender, EventArgs e)
        {
            // Get the name of the button and split it when there is a space
            string name = ((Button)sender).Name;
            string[] split = name.Split(new Char[] { ' ' });
            
            ButtonPress(Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));

            UpdateBoard();
        }

        public Board.Player GetActivePlayer()
        {
            if (game.currentState == BaseGame.GameStates.Player1Turn)
                return Board.Player.One;

            else
                return Board.Player.Two;
        }

        public void UpdateBoard()
        {
            switch (game.currentState)
            {
                case BaseGame.GameStates.StartGame:
                    gameDisplay.Text = "Setting Up Game";
                    game.currentState = BaseGame.GameStates.Player1Turn;
                    goto case BaseGame.GameStates.Player1Turn;
                //break;

                case BaseGame.GameStates.Player1Turn:
                    gameDisplay.Text = "Player 1's Turn";
                    gameDisplay.BackColor = Color.White;
                    gameDisplay.ForeColor = Color.Black;
                    break;

                case BaseGame.GameStates.Player2Turn:
                    gameDisplay.Text = "Player 2's Turn";
                    gameDisplay.BackColor = Color.Black;
                    gameDisplay.ForeColor = Color.White;
                    break;

                case BaseGame.GameStates.Player1Win:
                    gameDisplay.Text = "Player 1 Wins!";
                    gameDisplay.BackColor = Color.White;
                    gameDisplay.ForeColor = Color.Black;
                    playAgainButton.Enabled = true;
                    playAgainButton.Visible = true;
                    break;

                case BaseGame.GameStates.Player2Win:
                    gameDisplay.Text = "Player 2 Wins!";
                    gameDisplay.BackColor = Color.Black;
                    gameDisplay.ForeColor = Color.White;
                    playAgainButton.Enabled = true;
                    playAgainButton.Visible = true;
                    break;

                case BaseGame.GameStates.DrawGame:
                    gameDisplay.Text = "Draw!";
                    gameDisplay.BackColor = Color.Gray;
                    break;

                case BaseGame.GameStates.PlayAgain:
                    game.TheBoard.Initialise();
                    game.currentState = BaseGame.GameStates.StartGame;
                    goto case BaseGame.GameStates.StartGame;
                //break;
            }

            // Get the text to show in the relevent text boxes
            game.TheBoard.GetAvailableMoves(GetActivePlayer());
            gameInfo.Text = "Available Moves: " + game.TheBoard.availableMoves;

            playerScores.Text = game.TheBoard.GetPlayerPieceCount(Board.Player.One) +
                                " : " + game.TheBoard.GetPlayerPieceCount(Board.Player.Two);

            // Set the colour of each space depending on who it belongs to
            for (int x = 0; x < boardSize; x++)
            {
                for (int y = 0; y < boardSize; y++)
                {
                    if (game.currentState == BaseGame.GameStates.StartGame)
                    {
                        board[x, y].ForeColor = Color.Green;
                        board[x, y].Text = "";
                    }

                    else
                    {
                        if (game.TheBoard.board[x, y] == Board.Player.One)
                        {
                            board[x, y].ForeColor = Color.White;
                            board[x, y].Text = "O";
                        }

                        else if (game.TheBoard.board[x, y] == Board.Player.Two)
                        {
                            board[x, y].ForeColor = Color.Black;
                            board[x, y].Text = "O";
                        }

                        else if (game.TheBoard.boardState[x, y] == Board.BoardState.Available)
                        {
                            board[x, y].ForeColor = Color.DarkGreen;
                            board[x, y].Text = "O";
                        }

                        else
                        {
                            board[x, y].ForeColor = Color.Green;
                            board[x, y].Text = "";
                        }
                    }
                }
            }
        }

        /* Function called whenever a button is pressed */
        public void ButtonPress(int x, int y)
        {
            if (game.currentState == BaseGame.GameStates.Player1Turn)
            {
                try
                {
                    if (game.TheBoard.IsTurnValid(Board.Player.One, x, y))
                        game.TheBoard.PlacePiece(Board.Player.One, x, y);

                    else
                        return;

                    game.currentState = BaseGame.GameStates.Player2Turn;

                    if (game.TheBoard.IsWon() == true)
                    {
                        game.currentState = BaseGame.GameStates.Player1Win;
                    }
                    else
                    {
                        if (game.TheBoard.IsGameADraw() == true)
                        {
                            game.currentState = BaseGame.GameStates.DrawGame;
                        }
                        else
                        {
                            game.currentState = BaseGame.GameStates.Player2Turn;
                        }
                    }
                }

                catch (System.Exception)
                {

                }
            }

            else if (game.currentState == BaseGame.GameStates.Player2Turn)
            {
                try
                {
                    if (game.TheBoard.IsTurnValid(Board.Player.Two, x, y))
                        game.TheBoard.PlacePiece(Board.Player.Two, x, y);

                    else
                        return;

                    game.currentState = BaseGame.GameStates.Player1Turn;

                    if (game.TheBoard.IsWon() == true)
                    {
                        game.currentState = BaseGame.GameStates.Player2Win;
                    }
                    else
                    {
                        if (game.TheBoard.IsGameADraw() == true)
                        {
                            game.currentState = BaseGame.GameStates.DrawGame;
                        }
                        else
                        {
                            game.currentState = BaseGame.GameStates.Player1Turn;
                        }
                    }
                }

                catch (System.Exception)
                {

                }
            }
        }

        private void playAgainButton_Click(object sender, EventArgs e)
        {
            game.currentState = BaseGame.GameStates.PlayAgain;
            UpdateBoard();
            UpdateBoard();
            playAgainButton.Enabled = false;
            playAgainButton.Visible = false;
        }
    }
}
