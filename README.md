# README #

## Summary ##

Reversi is a strategy board game for two players, played on an 8×8 uncheckered board. There are sixty-four identical game pieces called disks (often spelled "discs"), which are light on one side and dark on the other. Players take turns placing disks on the board with their assigned color facing up. During a play, any disks of the opponent's color that are in a straight line and bounded by the disk just placed and another disk of the current player's color are turned over to the current player's color.

The object of the game is to have the majority of disks turned to display your color when the last playable empty square is filled.

(Source: [Wikipedia](http://en.wikipedia.org/wiki/Reversi))


## Contents of the project ##

There are two versions of Reversi included in this project:
* Standalone: Offline version playable between two local players
* Networked: Online version playable using the included Server, which can handle multiple games/clients.

A folder containing all required executables is included with each version, to allow quick access to the playable version. Found at "\Reversi - Standalone\Executables" for the standalone version and "\Reversi - Networked\Executables" for the networked version.

Also included in the project are various diagrams (Class, Game State, Network Flow) and an Introspective about the project.

## How do I get set up? ##

This application has been created and tested using Microsoft Visual Studio 2010 in Windows. Compatibility with other compilers and/or operating systems cannot be commented on.

### Standalone ###
The standalone version is a local game so can simply be run independently using either the executable or by building the VS solution.

### Networked ###
The networked version included two projects, Server and WinformsClient.
The server is a simple console application that outputs its address and port, as well as all interactions being passed through it. To change the address/port, the user must alter the Program.cs main function within the Server project. 

To start a game, the server must be running already. When the client is run, it will require the user to input the server address and port to connect to. The default values for both client and server is the local computer, to allow quick and easy access and play. Once an opponent is found, the game will begin.